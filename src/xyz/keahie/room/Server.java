package xyz.keahie.room;

import xyz.keahie.room.rooms.RoomManager;
import xyz.keahie.room.utils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    private List<ServerConnection> serverConnections = new ArrayList<>();
    private int port;

    private String protocolNumber;

    private RoomManager roomManager;

    public static void main(String[] args) throws IOException {
        new Server();
    }

    public Server() throws IOException {

        ServerSocket serverSocket;
        try {
            this.protocolNumber = "28d04w18y";
            port = FileUtils.getInteger(new File("config.txt"), "port");
            this.roomManager = new RoomManager(this);
            serverSocket = new ServerSocket(port);
            System.out.println("Starting server on port " + port);
        } catch (BindException e) {
            System.out.println("There is already a server running on port " + port + "!");
            return;
        }
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                ServerConnection serverConnection = new ServerConnection(socket, this);
                serverConnection.start();
                serverConnections.add(serverConnection);
                System.out.println("[Joined] " + serverConnections.size());
            } catch (Exception e) {
                break;
            }
        }
    }

    public List<ServerConnection> getServerConnections() {
        return serverConnections;
    }

    public RoomManager getRoomManager() {
        return roomManager;
    }

    public int getPort() {
        return port;
    }

    public String getProtocolNumber() {
        return protocolNumber;
    }
}
