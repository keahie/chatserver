package xyz.keahie.room;

import xyz.keahie.room.rooms.Room;
import xyz.keahie.room.rooms.RoomConnection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ServerConnection extends Thread {

    // TODO
    // Protocol Number Check for Client Server verification
    // Server checks version of client and let him in or not

    private Socket socket;
    private Server server;

    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;
    private boolean run;

    public ServerConnection(Socket socket, Server server) {
        this.socket = socket;
        this.server = server;

        this.run = true;
    }

    public void sendMessageToClient(String message) throws IOException {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        dataOutputStream.writeUTF(message);
        dataOutputStream.flush();
    }

    private void sendMessageToAllClients(String message) throws IOException {
        for (ServerConnection serverConnection : server.getServerConnections()) {
            serverConnection.sendMessageToClient(message);
        }
    }

    @Override
    public void run() {
        try {
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataInputStream = new DataInputStream(socket.getInputStream());

            while (run) {

                while (dataInputStream.available() == 0) {
                    Thread.sleep(1);
                }

                String input = dataInputStream.readUTF();
                String[] args = input.split(" ");
                System.out.println("[Server] Got message: " + input);
                switch (args[0].toLowerCase()) {
                    case "disconnect":
                        server.getServerConnections().remove(this);
                        System.out.println("[Left] " + server.getServerConnections().size());
                        break;
                    case "createroom":
                        String name = args[1];
                        server.getRoomManager().createRoom(name);
                        System.out.println("Room \"" + name + "\" was created");
                        sendMessageToAllClients("[~Client~] newroom " + name + " " + server.getRoomManager().getRooms().size());
                        break;
                    case "joinroom":
                        if (!server.getRoomManager().isUserInARoom(socket)) {
                            server.getServerConnections().remove(this);
                            System.out.println("[Left] " + server.getServerConnections().size());
                            for (Room room : server.getRoomManager().getRooms()) {
                                if (room.getRoomName().toLowerCase().equals(args[1].toLowerCase())) {
                                    server.getRoomManager().joinRoom(room, this, args[2]);
                                }
                            }
                        }
                        break;
                    case "getallrooms":
                        for (int i = 0; i < server.getRoomManager().getRooms().size(); i++) {
                            Room room = server.getRoomManager().getRooms().get(i);
                            sendMessageToClient("[~Client~] newroom " + room.getRoomName());
                        }
                        break;
                    case "sendprotocol":
                        sendMessageToClient("[~Checking~] " + (args.length == 2 && args[1].equals(server.getProtocolNumber())));
                        break;
                }
            }

            dataOutputStream.close();
            dataInputStream.close();
            socket.close();

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }
}
