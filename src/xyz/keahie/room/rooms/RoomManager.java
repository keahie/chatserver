package xyz.keahie.room.rooms;

import xyz.keahie.room.Server;
import xyz.keahie.room.ServerConnection;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class RoomManager {

    private List<Room> rooms;
    private int nextFreePort;

    public RoomManager(Server server) {

        this.nextFreePort = server.getPort() + 1;
        this.rooms = new ArrayList<>();
        createRoom("Plauderecke");
    }

    public void createRoom(String name) {
        Room room = new Room(name, nextFreePort);
        nextFreePort++;
        rooms.add(room);
        room.start();
    }

    public void joinRoom(Room room, ServerConnection serverConnection, String username) {
        if (!room.getUsernames().contains(username)) {
            try {
                serverConnection.sendMessageToClient("[~Client~] roomjoined " + room.getRoomName() + " " + room.getPort());
                room.getUsernames().add(username);
                room.sendBroadcastMessage(username + " joined the room\n");
            } catch (IOException e) {
                System.out.println("Something went wrong when " + serverConnection.getSocket().getInetAddress() + ":" + serverConnection.getSocket().getPort() + " tries to join " + room.getRoomName());
            }
        } else {
            try {
                serverConnection.sendMessageToClient("[Room - " + room.getRoomName() + "] There is already someone with the name " + username);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isUserInARoom(Socket socket) {
        for (Room room : getRooms()) {
            for (RoomConnection roomConnection : room.getRoomConnections()) {
                if (roomConnection.getSocket() == socket) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<RoomConnection> getAllConnections(Room room) {
        return room.getRoomConnections();
    }

    public List<Room> getRooms() {
        return rooms;
    }
}
