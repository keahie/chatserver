package xyz.keahie.room.rooms;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Room extends Thread {

    private List<RoomConnection> roomConnections;
    private String roomName;

    private List<String> usernames;
    private int port;

    public Room(String roomName, int port) {
        this.roomConnections = new ArrayList<>();
        this.roomName = roomName;
        this.usernames = new ArrayList<>();
        this.port = port;
    }

    @Override
    public void run() {
        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Starting room on port " + port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (true) {
            try {
                Socket socket = serverSocket.accept();
                RoomConnection roomConnection = new RoomConnection(socket, this);
                roomConnection.start();

                roomConnections.add(roomConnection);
                System.out.println("[" + roomName + " - Joined] " + roomConnections.size());
            } catch (Exception e) {
                break;
            }
        }
    }

    public void sendBroadcastMessage(String message) {
        for (RoomConnection roomConnection : roomConnections) {
            try {
                roomConnection.sendMessageToClient(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public List<RoomConnection> getRoomConnections() {
        return roomConnections;
    }

    public String getRoomName() {
        return roomName;
    }

    public int getPort() {
        return port;
    }

    public List<String> getUsernames() {
        return usernames;
    }
}
