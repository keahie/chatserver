package xyz.keahie.room.rooms;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class RoomConnection extends Thread {

    private Socket socket;
    private Room room;

    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;
    private boolean run;

    public RoomConnection(Socket socket, Room room) {
        this.socket = socket;
        this.room = room;

        this.run = true;
    }

    public void sendMessageToClient(String message) throws IOException {
        dataOutputStream.writeUTF(message);
        dataOutputStream.flush();
    }

    private void sendMessageToAllClients(String message) throws IOException {
        for (RoomConnection roomConnection : room.getRoomConnections()) {
            roomConnection.sendMessageToClient(message);
        }
    }

    @Override
    public void run() {
        try {
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataInputStream = new DataInputStream(socket.getInputStream());

            while (run) {

                while (dataInputStream.available() == 0) {
                    Thread.sleep(1);
                }

                String input = dataInputStream.readUTF();
                System.out.println("[Room - " + room.getRoomName() + "] Got message: " + input);
                if (input.toLowerCase().contains("[~room~]")) {
                    String[] args = input.split(" ");
                    if ("disconnect".equals(args[1].toLowerCase())) {
                        room.getRoomConnections().remove(this);
                        room.getUsernames().remove(args[3]);
                        sendMessageToClient("[~Client~] roomleft " + room.getRoomName() + " " + args[2]);
                        sendMessageToAllClients(args[3] + " left the room\n");
                        System.out.println("[" + room.getRoomName() + " - Left] " + room.getRoomConnections().size());
                    }
                    break;
                }

                System.out.println("Got message from " + socket.getInetAddress().toString().substring(1) + ":" + socket.getPort() + " -> " + input);
                if (input.length() > 200 || input.trim().length() <= 0) {
                    sendMessageToClient("Your message is too long! (maximum of 200 words)");
                    continue;
                }
                sendMessageToAllClients(input);
            }

            dataOutputStream.close();
            dataInputStream.close();
            socket.close();

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }
}
